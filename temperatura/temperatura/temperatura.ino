#include <LiquidCrystal.h>

// iniciamos la libreria con los numeros de los pines de la interfaz
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
const int relay = 7;

int reading;
int oldReading;

int flag = 1;
int celsius = 0;

void setup() {
  // configuramos el numero de columnas y filas del LCD:
  pinMode(relay, OUTPUT);
  lcd.begin(16, 2);
  //digitalWrite(relay, LOW);//lampara on / led off
}

void loop() {
  reading = analogRead(0);
  
  if(flag == 1){
    celsius = reading/2; 
  }else if(reading > (oldReading + 4) || flag == 0){
    //celsius = (reading/2) - 10; //USB
    celsius = reading/3; //Pilas    
  }
  
  //head
  lcd.setCursor(0, 0);
  lcd.print(celsius, DEC);
  //lcd.print((char)223);
  lcd.print(" C");
  
  //footer tes
  
  lcd.print(flag);
  lcd.print(" ");
  lcd.print(String(analogRead(0), DEC));
  
  lcd.setCursor(0, 1);
  if(celsius <= 20){
    digitalWrite(relay, LOW);//lampara on / led off
    flag = 1;
    lcd.print("baja");
    //delay(10000);
  }
  else{
    digitalWrite(relay, HIGH);//lampara off / led on
    flag = 0;
    lcd.print("normal");
    //delay(10000);
  }
  
  //fin
  oldReading = reading;
  lcd.print(" ");
  789lcd.print(oldReading);
  delay(5000);
  lcd.clear();
}
