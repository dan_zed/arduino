#include <LCD5110_Graph.h>
#include "DHT.h"

LCD5110 lcd(8,9,10,12,11);

#define DHTPIN 2     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);

extern uint8_t temperatureIcon[];
extern uint8_t splash[];
extern uint8_t SmallFont[];
extern unsigned char MediumNumbers[];

char numberString[6];

void setup() {
  Serial.begin(9600); 
  // put your setup code here, to run once:
  dht.begin();
  
  lcd.InitLCD();
  lcd.clrScr();
  lcd.drawBitmap(0, 0, splash, 84, 48);
  lcd.update();
  lcd.setFont(MediumNumbers);
  Serial.begin(9600); 
  delay(2000);
}

void loop() {
  // put your main code here, to run repeatedly:
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    Serial.println("nada");
  } else {    
    lcd.clrScr();
    lcd.drawBitmap(0, 0, temperatureIcon, 84, 48);
    
    //Temperatura
    convertToString(t);    
    lcd.print(numberString,22,29);
    Serial.println(t);
    
    //Humedad
    convertToString(h);
    lcd.print(numberString,22,5);
    
    lcd.update();
  }
  
  delay(2000);
}

void convertToString(float number)
{
   dtostrf(number, 3, 1, numberString);
}
