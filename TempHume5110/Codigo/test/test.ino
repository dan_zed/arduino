#include <LCD5110_Graph.h>

LCD5110 lcd(8,9,10,12,11);

extern unsigned char BigNumbers[];
extern uint8_t temperatureIcon[];
extern uint8_t splash[];
extern uint8_t SmallFont[];

char temperatureF[6];

void setup() {
  lcd.InitLCD();
  lcd.clrScr();
  lcd.drawBitmap(0, 0, splash, 84, 48);
  lcd.update();
  delay(2000);
  lcd.setFont(SmallFont);

  lcd.clrScr();
  lcd.drawBitmap(0, 0, temperatureIcon, 84, 48);
  convertToString(25.14);
  lcd.print(temperatureF,22,10);
  lcd.update();
}

void loop() {
  // put your main code here, to run repeatedly:

}

void convertToString(float number)
{
   dtostrf(number, 3, 1, temperatureF);
}
