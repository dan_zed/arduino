#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include "DHT.h"

#define DHTPIN 2     // what pin we're connected to
 
// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE);

Adafruit_PCD8544 display = Adafruit_PCD8544(8,9,10,11,12);

void setup() {
  Serial.begin(9600); 
  Serial.println("DHTxx test!");
 
  dht.begin();
  
  display.begin();
  // init done

  // you can change the contrast around to adapt the display
  // for the best viewing!
  display.setContrast(50);

  //display.display(); // show splashscreen
  delay(2000);
  display.clearDisplay();   // clears the screen and buffer
  
  //Humedad
  display.setTextColor(BLACK);
  display.println("============");
  display.println("Humedad");
  display.setTextColor(WHITE, BLACK); // 'inverted' text
  display.println("60 %");
  display.display();
}

void loop() {
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  float t = dht.readTemperature();
 
  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from DHT");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" %\t");
    
    Serial.print("Temperature: "); 
    Serial.print(t);
    Serial.println(" *C");
    
    display.clearDisplay();
    
    //Temperatura
    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(0,0);
    display.println("Temperatura");
    display.setTextColor(WHITE, BLACK); // 'inverted' text
    display.print(t);
    display.print(" C");
    display.print((char)247);
    
    //Humedad
    display.setTextColor(BLACK);
    display.setCursor(0,24);
    display.println("");
    display.println("Humedad");
    display.setTextColor(WHITE, BLACK); // 'inverted' text
    display.print(h);
    display.print("%");
    display.display();
  }
  delay(2000);
  
}
