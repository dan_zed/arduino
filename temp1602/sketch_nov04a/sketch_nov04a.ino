/*
Library Originally added by David A Mellis
Library Modified by Limor Fried
Example added by Tom Igoe
Modified by Tom Igoe
This example is in the public domain. http://www.arduino.cc.en/Tutorial/LiquidCrystal
Contrast modification by Ahmed Murtaza Qureshi (www.engineeringlearning.blogspot.com)
*/
#include <LiquidCrystal.h>
char ch;
int Contrast=105;
int estadoLuz = 0;
int estadoAnteriorLuz = 0;
int salidaLuz = 0;
int tempPin = 0; // Definimos la entrada en pin A0
float tempC;
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() 
{
  Serial.begin(9600);
  Serial.println("LCD test with PWM contrast adjustment");
  pinMode(10,OUTPUT);
  analogWrite(6,Contrast);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("LCD test!!");
  //analogWrite(9,28836);
  pinMode(8,INPUT);
}

void loop() 
{
  //codigo Test
  /*digitalWrite(10, HIGH);
  delay(5000);
  digitalWrite(10, LOW);
  analogWrite(9,28836);*/
  
  // Lee el valor desde el sensor
  tempC = analogRead(tempPin); 

  // Convierte el valor a temperatura
  tempC = (5.0 * tempC * 100.0)/1024.0;
  lcd.begin(0,1);
  lcd.print(tempC);
  lcd.print(" Celsius");
  
  //estadoLuz = digitalRead(8);
  //Serial.println(estadoLuz);
  analogWrite(9,28836);

  if(tempC < 22 && estadoAnteriorLuz == 0){
    digitalWrite(10, HIGH);
    estadoAnteriorLuz = 1;
    Serial.println(tempC);
  }else if(tempC > 25 && estadoAnteriorLuz == 1){
    digitalWrite(10, LOW);
    estadoAnteriorLuz = 0;
    Serial.println(tempC);
    Serial.println("low");
  }

  delay(2000);
}

/*void serialEvent()
{
    if (Serial.available())
    {
      ch= Serial.read();
      Serial.println(ch);
      if(ch=='A' && Contrast<255)
      {
        Contrast=Contrast+1;
      }
      if(ch=='B' && Contrast>0)
      {
        Contrast=Contrast-1;
      }
      if(ch=='N')
      {
        analogWrite(9,28836);
      }
      if(ch=='F')
      {
        analogWrite(9,0);
      }
      analogWrite(6,Contrast);
      Serial.println("Current contrast");
      Serial.println(Contrast);
    }
}*/
